# Example suite

## Transform a result
Usage:
```
TabularTranform [-q|-e] input.nt
	-q extract per-query metrics (default)
	-e extract per-execution metrics
```
Example:
`java -cp target/iguana-3.3.3.jar org.aksw.iguana.rp.analysis.TabularTransform  -q wdqs-example-suite/result.nt`

### Output columns:
#### Per query metrics:
- endpointLabel: name of the connection defined in the suite configuration under ***connection***.
- task: identifier of the task being run
- queryId: the query ID
- totalTime: the total time spend running this query successfully (in ms)
- success: the number of times this query was run and returned successfully
- failed: the number of times this query failed
- timeouts: the number of times this query hit a timeout (detected by IGUANA)
- resultSize: the maximum result size
- unknownException: the number of times this query hit a an unknownException
- wrongCodes: the number of times this query hit an IOException or non-200 HTTP return code
- qps: successful queries per second (`success/(totalTime/1000)`)
- penalizedQPS: penalized QPS metrics (`(success+failed)/(penalizedTime/1000)`, where `penalizedTime = totalTime + failed*timePenalty`)

#### Per execution metrics:
- endpointLabel: name of the connection defined in the suite configuration under ***connection***.
- task: identifier of the task being run
- taskStartDate: start time
- taskEndDate: end time
- successfullQueries: total number of successful queries
- successfullQueriesQPH: total number of succesul queries per hour
- avgqps: average of per-query qps
- penalizedAvgQPS: average of per-query penalizedQPS
- queryMixesPH: successfullQueriesQPH / number of distinct queries
