package org.aksw.iguana.rp.analysis;

import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionLocal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class TabularTransform implements AutoCloseable {
    private final static String PER_EXEC_METRICS_SPARQL = readResource("/per_execution_metric.sparql");
    private final static String PER_QUERY_METRICS_SPARQL = readResource("/per_query_metric.sparql");

    private static String readResource(String resource) {
        try (InputStream is = TabularTransform.class.getResourceAsStream(resource)) {
            if (is == null) {
                throw new RuntimeException(resource + " not found");
            }
            return new String(is.readAllBytes(), StandardCharsets.UTF_8);
        } catch (IOException ioe) {
            throw new RuntimeException("Cannot read " + resource, ioe);
        }
    }

    private final RDFConnection connection;

    public TabularTransform(RDFConnection connection) {
        this.connection = connection;
    }

    public void queryToCSV(String sparql, OutputStream os) {
        try(QueryExecution qe = connection.query(sparql)) {
            ResultSet resultSet = qe.execSelect();
            ResultSetFormatter.outputAsCSV(os, resultSet);
        }
    }

    public static TabularTransform fromFile(String path) {
        Model m = ModelFactory.createDefaultModel();
        m.read(path);
        Dataset ds = DatasetFactory.create(m);
        return new TabularTransform(new RDFConnectionLocal(ds));
    }
    public static void main(String[] args) throws Exception {
        String filename = null;
        String sparlQuery = PER_QUERY_METRICS_SPARQL;
        if (args.length > 2 || args.length < 1) {
            usage();
        }
        for (String s: args) {
            switch (s) {
                case "-q":
                    sparlQuery = PER_QUERY_METRICS_SPARQL;
                    break;
                case "-e":
                    sparlQuery = PER_EXEC_METRICS_SPARQL;
                    break;
                default:
                    if (filename != null) {
                        usage();
                    }
                    filename = s;
            }
        }
        if (filename == null) {
            usage();
        }

        try (TabularTransform tt = TabularTransform.fromFile(filename)) {
            tt.queryToCSV(sparlQuery, System.out);
        }
    }

    private static void usage() {
        System.err.println("TabularTransform [-q|-e] input.nt");
        System.err.println("\t-q extract per-query metrics (default)");
        System.err.println("\t-e extract per-execution metrics");
        System.exit(1);
    }

    @Override
    public void close() throws Exception {
        connection.close();
    }
}
